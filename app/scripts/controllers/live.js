'use strict';

angular.module('reactionsliveApp')
  .controller('LiveCtrl', function ($scope, $routeParams, Server) {

    var designReactions = null;
    $scope.inputs = {};
    $scope.comments = [];
    //Get the campaign design
    Server.get('campaign?_id='+$routeParams.campaignID).then(function(response){
        $scope.design = response.data;
        console.log($scope.design)
        $scope.template = 'views/templates/template'+String(response.data.template)+'.html';
        // designReactions = $scope.design.fields.map(function(el){return el.reaction;});
        // designReactions.forEach(function(el){
        //     $scope.reactions[el] = {count: 0, percentage: 0};
        // })
        // setInterval(refreshCounts, 3000);
        $scope.design.fields.forEach(function(el){
            $scope.inputs[el.input] = {count:0, percentage:0};
        })
        setInterval(getComments, 3000);
    })


    
    // $scope.reactions = {};
    //And then get the reactions
    // var refreshCounts = function(){
    //     console.log('refreshCounts')
    //     Server.get('reactionCounts?_id='+$routeParams.campaignID).then(function(response){
    //         var reactionCounts = response.data;
    //         var sum = designReactions.map(function(el){return reactionCounts[el]}).reduce(function(a, b){return a + b;}, 0);
    //         sum = sum>0 ? sum : 1;
    //         designReactions.forEach(function(el){
    //             $scope.reactions[el].count = reactionCounts[el];
    //             $scope.reactions[el].percentage = 100.0*reactionCounts[el]/sum;
    //         })
            
    //     })
    // }

    var getComments = function(){
        Server.get('comments?_id='+$routeParams.campaignID).then(function(response){

            var comments = response.data.map(function(el){return el.message});
            var sum = 0
            $scope.design.fields.forEach(function(el){
                var value = 0
                for (var i = 0; i < comments.length; i++) {
                  if (comments[i].indexOf(el.input)>-1) {
                    value++;
                  }
                }
                $scope.inputs[el.input].count = value;
                sum += value;
            })


            for (var property in $scope.inputs) {
                if ($scope.inputs.hasOwnProperty(property)) {
                    $scope.inputs[property].percentage = 100*$scope.inputs[property].count/sum;
                }
            }


        })
    }

    $scope.getPercentageStyle = function(option){
        if(['1','1c'].indexOf($scope.design.template)>-1){return {height: String($scope.inputs[option].percentage)+'%'};}
        else if($scope.design.template==2){return {width: String($scope.inputs[option].percentage)+'%'};}
        else{return null;}
    }

    // $scope.getPercentageStyle = function(reaction){
    //     if($scope.design.template==1){return {height: String($scope.reactions[reaction].percentage)+'%'};}
    //     else if($scope.design.template==2){return {width: String($scope.reactions[reaction].percentage)+'%'};}
    //     else{return null;}
    // }

    
    
    

});
