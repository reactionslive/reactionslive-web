'use strict';

angular.module('reactionsliveApp')
  .controller('DoneCtrl', function ($scope, $routeParams, Server, $sce) {


    Server.get('campaign?_id='+$routeParams.campaignID).then(function(response){
        $scope.design = response.data;
        $scope.template = 'views/templates/template'+String(response.data.template)+'.html';
    })

    Server.get('pages').then(function(response){
        $scope.pages = response.data;
    })

    $scope.getPercentageStyle = function(){
        if(['1','1c'].indexOf($scope.design.template)>-1){return {height: '50%'};}
        else if($scope.design.template==2){return {width: '50%'};}
        else{return null;}
    }

    $scope.page = null;
    $scope.duration = null;
    $scope.description = null;
    $scope.state = 0;
    $scope.startStream = function(){
        $scope.state = 1;
        var streamdata = {
            pageurl: "http://www.reactionslive.com/#/live/"+$routeParams.campaignID,
            pageID: $scope.pages[$scope.page].id,
            campaignID: $routeParams.campaignID,
            duration: Number($scope.duration),
            description: $scope.description
        };
        console.log('streamdata: ', streamdata)
        Server.post('startStream', streamdata)
        .then(function(response){
            console.log(response.data);
            $scope.state = 2; 
            // $scope.player = response.data; //$sce.trustAsHtml(response.data);
            // // $("player_wrapper").html(response.data)
        })

    }

});
