'use strict';

/**
 * @ngdoc service
 * @name reactionsliveApp.Auth
 * @description
 * # Auth
 * Service in the reactionsliveApp.
 */
angular.module('reactionsliveApp')
  .service('Auth', ['$q', 'Server', function($q, Server){
    
    //Should save user data here so we don't have to get it every time from server

    // Don't forget to comment this again if you uncomment!
    // this.user = 'default';

    //Reject this promise if not logged in
    this.requireAuth = function(){

      if(this.user){
        return $q.when(this.user);
      }
      else{
        return $q(function(resolve, reject){
          Server.get("loggedin")
                  .then(function(user){ 
            
            if (user.data !== '0') {
              resolve(user.data);
            }
            else{
              reject('AUTH_REQUIRED');
            }
          });
        });
      }
      
    }

    //This just returns the current user data ("0" if not logged in)
    this.getAuth = function(){
      if(this.user){
        return $q.when(this.user);
      }
      else{
        return Server.get("loggedin")
                  .then(function(user){
          return user;
        });
      }
    }

  }])