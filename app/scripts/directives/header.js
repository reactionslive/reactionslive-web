'use strict';

/**
 * @ngdoc directive
 * @name reactionsliveApp.directive:header
 * @description
 * # header
 */
angular.module('reactionsliveApp')
  .directive('header', function () {
      return {
          restrict: 'A', //Use like <div header user=.. >
          replace: true,
          scope: {}, 
          templateUrl: "views/header.html",
          controller: ['$scope','Auth', 'Server', function ($scope, Auth, Server) {

            $scope.serverhost = Server.host;

            Auth.getAuth().then(function(user){
              $scope.user = user.data;
            })



          }]
      }
  });
