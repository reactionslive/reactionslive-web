'use strict';

angular.module('reactionsliveApp')
  .controller('DoneCtrl', function ($scope, $routeParams, Server, $sce) {


    Server.get('campaign?_id='+$routeParams.campaignID).then(function(response){
        $scope.design = response.data;
        $scope.template = 'views/templates/template'+String(response.data.template)+'.html';
    })

    Server.get('pages').then(function(response){
        $scope.pages = response.data;
    })

    $scope.getPercentageStyle = function(){
        return {height: '50%'};
    }

    $scope.page = null;
    $scope.startStream = function(){

        var streamdata = {
            pageID: $scope.pages[$scope.page].id,
            campaignID: $routeParams.campaignID
        };
        console.log('streamdata: ', streamdata)
        Server.post('startStream', streamdata)
        .then(function(response){
            console.log(response.data);
            // $scope.player = response.data; //$sce.trustAsHtml(response.data);
            // // $("player_wrapper").html(response.data)
        })

    }

});
