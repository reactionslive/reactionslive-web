'use strict';

/**
 * @ngdoc service
 * @name reactionsliveApp.Server
 * @description
 * # Server
 * Service in the reactionsliveApp.
 */
angular.module('reactionsliveApp')
  .service('Server', function($http){

    this.host = "http://52.212.34.208:3000/"; //52.212.34.208
    this.get = function(path){
      return $http({
                method: 'GET',
                withCredentials: true,
                url: this.host+path})
    }
    this.post = function(path,body){
      return $http({
                method: 'POST',
                withCredentials: true,
                url: this.host+path,
                data: body})
    }

  })
