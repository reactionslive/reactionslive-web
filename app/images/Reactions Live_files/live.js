'use strict';

angular.module('reactionsliveApp')
  .controller('LiveCtrl', function ($scope, $routeParams, Server) {

    var designReactions = null;
    $scope.reactions = {};
    //Get the campaign design
    Server.get('campaign?_id='+$routeParams.campaignID).then(function(response){
        $scope.design = response.data;
        $scope.template = 'views/templates/template'+String(response.data.template)+'.html';
        designReactions = $scope.design.fields.map(function(el){return el.reaction;});
        designReactions.forEach(function(el){
            $scope.reactions[el] = {count: 0, percentage: 0};
        })
        setInterval(refreshCounts, 3000);
    })


    
    $scope.reactions = {};
    //And then get the reactions
    var refreshCounts = function(){
        console.log('refreshCounts')
        Server.get('reactionCounts?_id='+$routeParams.campaignID).then(function(response){
            var reactionCounts = response.data;
            var sum = designReactions.map(function(el){return reactionCounts[el]}).reduce(function(a, b){return a + b;}, 0);
            sum = sum>0 ? sum : 1;
            designReactions.forEach(function(el){
                $scope.reactions[el].count = reactionCounts[el];
                $scope.reactions[el].percentage = 100.0*reactionCounts[el]/sum;
            })
            

        })
    }

    $scope.getPercentageStyle = function(reaction){
        return {height: String($scope.reactions[reaction].percentage)+'%'}
    }

    
    
    

});
