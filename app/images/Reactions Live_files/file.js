'use strict';

/**
 * @ngdoc directive
 * @name reactionsliveApp.directive:file
 * @description
 * # file
 */
angular.module('reactionsliveApp')
  .directive('file', function() {
    return {
        restrict: 'E',
        template: '<input type="file" />',
        replace: true,
        require: 'ngModel',
        link: function(scope, element, attr, ctrl) {
            var listener = function() {
                scope.$apply(function() {
                    attr.multiple ? ctrl.$setViewValue(element[0].files) : ctrl.$setViewValue(element[0].files[0]);
                });
            }
            element.bind('change', listener);
        }
    }
})
  .directive('fileuploadS3', function($http, Server) {
    return {
        restrict: 'E',
        template: '<input type="file" />',
        replace: true,
        require: 'ngModel',
        link: function(scope, element, attr, ctrl) {
            var listener = function() {

                var file = element[0].files[0];

                var setValue = function(value) {
                        ctrl.$setViewValue(value);
                        ctrl.$render();
                    }

                $http({
                        method: 'GET',
                        withCredentials: true,
                        url: Server.host+"getSignedUrl",
                        params: {filename: file.name}
                    })
                .then(function(response){

                    var signedUrl = response.data;
                    var xhr = new XMLHttpRequest();
                    xhr.file = file; // not necessary if you create scopes like this

                    xhr.onreadystatechange = function(e) {
                      if ( e.currentTarget.readyState == 4 ) {
                        // done uploading! HURRAY!
                        console.log('finished uploading: ', signedUrl.split('?')[0])
                        setValue(signedUrl.split('?')[0]);
                      }
                    };
                    xhr.open('PUT', signedUrl, true);
                    xhr.setRequestHeader("Content-Type","application/octet-stream");
                    xhr.setRequestHeader("enctype","application/multipart/formdata");
                    xhr.send(file);
                })

                
                
    
            }
            element.bind('change', listener);
        }
    }
});
