'use strict';

/**
 * @ngdoc function
 * @name reactionsliveApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the reactionsliveApp
 */
angular.module('reactionsliveApp')
  .controller('MainCtrl', function ($scope, $http, currentAuth, Server) {

    $scope.currentAuth = currentAuth.data;
    $scope.serverhost = Server.host;

    $scope.contact = null;
    $scope.email = function(){

        Server.post('email', $scope.contact).then(function(response){
                    console.log(response.data)
                    $scope.contact = null;
                    window.alert('Thank you for contacting Reactions Live, we will respond ASAP!')
                });
           
    }

  });
