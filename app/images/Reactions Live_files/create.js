'use strict';




angular.module('reactionsliveApp')
  .controller('CreateCtrl', function ($scope, $routeParams, $location, Server) {

    $scope.template = 'views/templates/template'+String($routeParams.templateID)+'.html';

    $scope.file=null;

    $scope.design = {
        numFields: "2",
        fields:[{"reaction":"like"},
                {"reaction":"love"},
                {"reaction":"haha"},
                {"reaction":"wow"}]};

    $scope.getPercentageStyle = function(){
        return {height: '50%'};
    }
    
    //Function to create an array for ng-repeat to iterate over
    $scope.getNumber = function(numStr) {
        return Array.apply(null, Array(parseInt(numStr))).map(function (_, i) {return i;});   
    }

    //Function to check for duplicates so user can't choose same reaction for different sliders
    $scope.hasDuplicates = function() {
        var array = $scope.design.fields.map(function(el){
            return el.reaction;
        }).slice(0,$scope.design.numFields);

        var valuesSoFar = Object.create(null);
        for (var i = 0; i < array.length; ++i) {
            var value = array[i];
            if (value in valuesSoFar) {
                return true;
            }
            valuesSoFar[value] = true;
        }
        return false;
    }
    

    $scope.saveCampaign = function(){
        
        var design ={
                template: $routeParams.templateID,
                title: $scope.design.title,
                numFields: $scope.design.numFields,
                fields: $scope.design.fields.slice(0, $scope.design.numFields).map(function(el){
                    delete el.$$hashKey;
                    return el;
                }),
                background: $scope.design.background
            };
            
        Server.post('campaign', design).then(function(response){
            $location.path('done/'+String(response.data._id));
        })


        
    }

  });
