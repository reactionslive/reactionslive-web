'use strict';

/**
 * @ngdoc overview
 * @name reactionsliveApp
 * @description
 * # reactionsliveApp
 *
 * Main module of the application.
 */

angular
  .module('reactionsliveApp', [
    'ngResource',
    'ngRoute'
  ])
  .config(function ($routeProvider, $locationProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        resolve: {
          "currentAuth": ["Auth", function(Auth) {
            return Auth.getAuth();
          }]
        }
      })
      .when('/create/:templateID', {
        templateUrl: 'views/create.html',
        controller: 'CreateCtrl',
        resolve: {
          "currentAuth": ["Auth", function(Auth) {
            return Auth.requireAuth();
          }]
        }
      })
      .when('/contact', {
        templateUrl: 'views/contact.html',
        controller: 'AboutCtrl'
      })
      .when('/done/:campaignID', {
        templateUrl: 'views/done.html',
        controller: 'DoneCtrl',
        resolve: {
          "currentAuth": ["Auth", function(Auth) {
            return Auth.requireAuth();
          }]
        }
      })
      .when('/live/:campaignID',{
        templateUrl:'views/live.html',
        controller: 'LiveCtrl'
      })
      .when('/choose', {
        templateUrl: 'views/choose.html',
        resolve: {
          "currentAuth": ["Auth", function(Auth) {
            return Auth.requireAuth();
          }]
        }
      })
      .when('/terms', {
        templateUrl: 'views/terms.html',
        controller: 'AboutCtrl'
      })
      .when('/privacypolicy', {
        templateUrl: 'views/privacypolicy.html'
      })
      .otherwise({
        redirectTo: '/'
      });

      // $locationProvider.html5Mode(true);

      
  })
  .run(function($rootScope, $location){

    $rootScope.$on("$routeChangeError", function(event, next, previous, error) {
        // We can catch the error thrown when the $requireSignIn promise is rejected
        // and redirect the user back to the home page
        if (error === "AUTH_REQUIRED") {
          $location.path("/");
          window.alert('You have to log in!')
        }
      });
  
 });
