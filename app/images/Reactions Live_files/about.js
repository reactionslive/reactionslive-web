'use strict';

/**
 * @ngdoc function
 * @name reactionsliveApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the reactionsliveApp
 */
angular.module('reactionsliveApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
